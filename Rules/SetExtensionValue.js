export default function SetExtensionValue(controlProxy) {
  console.log("In SetExtensionValue");
  let srcValue = controlProxy.getValue();
  let targetCtrl = controlProxy.evaluateTargetPath("#Page:Customer_Edit/#Control:MyExtensionControlName");
  targetCtrl.setValue(srcValue);
}